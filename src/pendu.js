const wordToFind = document.getElementById("wordToFind").innerHTML;
const findWord = document.getElementById("mot");
const letters = document.getElementById("word");
const sawLetters = document.getElementById("oldLetters");
const trial = document.getElementById("trial");
const difficulty = document.getElementById("difficulty").innerHTML;
const firstLetter = document.getElementById("firstLetter").innerHTML;
let doubleEssaies = document.getElementById("doubleEssaies").innerHTML;
let context;
let essaisManques = 0;
const canvas = document.getElementById('mon_canvas');
const modal = document.getElementById("modal-one");
const modal2 = document.getElementById("modal-two");

function addLetters() {
    sawLetters.innerHTML += `
<div class="letters">
 ${letters.value.toUpperCase()}
</div>
`
    letters.value = ""
}

function pendu() {
    const alerte = document.getElementById("alert");
    let lettersStorage = localStorage.getItem('wordUse');

    const lettersUse = letters.value.toUpperCase()
    const verif = lettersStorage.split("")
    if (verif.includes(lettersUse)) {
        alerte.innerHTML = "Lettre déja utilisé veuillez réessayer"
        return true
    }else if (lettersUse === "" ){
        alerte.innerHTML = "veuillez donner une lettre"
        return true
    } else {
        alerte.innerHTML = " "
        lettersStorage += lettersUse
    }
    localStorage.setItem('wordUse', lettersStorage);
    if (wordToFind.includes(lettersUse)) {
        findWord.innerHTML = wordFind()
    } else {
        let essaie = trial.innerHTML
        essaie = parseInt(essaie) - 1
        trial.innerHTML = essaie
        if (essaie === 0) {
            modal2.classList.add("open");
        }
        addCanvas()
    }

    addLetters()
}


function wordFind() {
    let lettersStorage = localStorage.getItem('wordUse');
    lettersStorage = lettersStorage.split("")
    let word = '';
    let find = 0
    for (let i = 0; i < wordToFind.length; i++) {
        if (lettersStorage.includes(wordToFind[i])) {
            word += wordToFind[i]
            find += 1
        } else {
            word += ' _ '
        }
    }
    if (find === wordToFind.length) {
        modal.classList.add("open");
    }
    return word
}


function startGame() {
    if (parseInt(difficulty) === 1) {
        if (doubleEssaies){
            trial.innerHTML = 11
        }else{
            trial.innerHTML = 10
        }
    } else if (parseInt(difficulty) === 2) {
        if (doubleEssaies){
            trial.innerHTML = 8
        }else{
            trial.innerHTML = 7
        }
    } else {
        if (doubleEssaies){
            trial.innerHTML = 6
        }else{
            trial.innerHTML = 5
        }
    }
    localStorage.clear()
    localStorage.setItem('wordUse', "");
    let puzzle = '';
    for (let i = 0; i < wordToFind.length; i++) {
        if (i === 0 && firstLetter){
            puzzle += wordToFind[i]
            localStorage.setItem('wordUse', wordToFind[i]);
        }else{
            puzzle += ' _ '
        }

    }
    findWord.innerHTML = puzzle
}


function addCanvas() {
    if (doubleEssaies && essaisManques === 0){
        doubleEssaies = false
        return true
    }else{
        essaisManques++
    }

    switch (essaisManques) {
        case 1:
            context.beginPath(); // On démarre un nouveau tracé
            context.lineCap = 'round';
            context.lineWidth = "10";

            context.lineJoin = 'round';
            context.strokeStyle = "rgb(23, 145, 167)";
            context.moveTo(35, 295);
            context.lineTo(5, 295);
            context.stroke();
            if (parseInt(difficulty) === 2 || parseInt(difficulty) >= 3) {
                addCanvas()
            }
            break
        case 2:
            context.moveTo(20, 295);
            context.lineTo(20, 5);
            context.stroke();
            if (parseInt(difficulty) === 2 || parseInt(difficulty) >= 3) {
                addCanvas()
            }
            break;
        case 3:
            context.lineTo(200, 5);
            context.stroke();
            if (parseInt(difficulty) >= 3) {
                addCanvas()
            }
            break;
        case 4:
            context.lineTo(200, 50);
            context.stroke();
            if (parseInt(difficulty) === 2 || parseInt(difficulty) >= 3) {
                addCanvas()
            }
            break;
        case 5:
            context.beginPath();
            context.fillStyle = "red";
            context.arc(200, 50, 20, 0, Math.PI * 2);
            context.fill();
            if (parseInt(difficulty) >= 3) {
                addCanvas()
            }
            break;
        case 6 :
            context.beginPath();
            context.strokeStyle = "red";
            context.moveTo(200, 50);
            context.lineTo(200, 150);
            context.stroke();
            break;
        case 7 :
            context.beginPath();
            context.moveTo(200, 80);
            context.lineTo(160, 110);
            context.stroke();
            break;
        case 8:
            context.beginPath();
            context.moveTo(200, 80);
            context.lineTo(240, 110);
            context.stroke();
            break;
        case 9:
            context.beginPath();
            context.moveTo(200, 150);
            context.lineTo(180, 200);
            context.stroke();
            break;
        case 10:
            context.beginPath();
            context.moveTo(200, 150);
            context.lineTo(220, 200);
            context.stroke();
            context.beginPath();
            context.fillStyle = "rgb(23, 145, 167)";
            context.arc(200, 62, 16, 0, Math.PI * 2);
            context.fill();
            context.beginPath();
            context.fillStyle = "red";
            context.arc(200, 50, 20, 0, Math.PI * 2);
            context.fill();
            break;

        default:
            essaisManques = 0;
            context.clearRect(0, 0, 300, 300);
    }
}

window.onload = function () {
    if (!canvas) {
        alert("Impossible de récupérer le canvas");
    } else {
        context = canvas.getContext('2d');
        if (!context) {
            alert("Impossible de récupérer le context du canvas");
        }
    }
}


function closeModal() {
    modal.classList.remove("open");
    document.location.href="/src/PageNiveaux.php";
    //redirection page d'accueil
}

function closeModal2() {
    modal2.classList.remove("open");
    document.location.href="/src/PageNiveaux.php";
    //redirection page d'accueil
}


startGame()