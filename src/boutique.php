<?php
session_start();
$nav_en_cours = 'boutique';

class shopItem
{
    public $id;
    public $name;
    public $price;
    public $description;
    public $image;

    function __construct($id, $name, $price, $description, $image)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->description = $description;
        $this->image = $image;
    }

    function buyItem()
    {
        $_SESSION['pieces'] -= $this->price;
        array_push($_SESSION['boughtItems'], $this->id);
    }
}

$shopItemList = array(
    new shopItem(1, 'item1', 30, "Permet l'affichage d'une lettre", '../img/firstLetter.png'),
    new shopItem(2, 'item2', 100, "Permet si on gagne le niveau de gagner le double de pièces", '../img/doubleCash.png'),
    new shopItem(3, 'item3', 50, "Ajout d'essai", '../img/essaisBonus.png'),
    // new shopItem(4, 'item4', 60, "Permet l'affichage de 3 lettres", 'https://assets.pinshape.com/uploads/user/avatar/800420/cover_9403.png'),
);

if (isset($_POST['id'])) {
    $i = array_search($_POST['id'], array_column($shopItemList, 'id'));
    $selectedItem = ($i !== false ? $shopItemList[$i] : null);
    $selectedItem->buyItem();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="navbar.css">
    <title>Boutique</title>
</head>

<body>
    <style>
        body {
            margin: 0;
            background-color: #780000;

        }

        section {
            display: flex;
            justify-content: space-around;

        }

        article {
            border-radius: 6px;
            background-color: white;
            width: 250px;
            text-align: center;
            box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, 0.2);
            display: grid;
            grid-template-rows: auto 1fr 1fr;
            margin-top: 50px;
        }

        img {

            margin: auto;
            padding: 20px;

        }

        .flex {
            display: flex;
            justify-content: space-between;
        }

        p {
            align-self: center;
            margin-right: 10px;
        }

        h3 {
            margin-left: 10px;
        }

        .info {
            text-align: center;
            margin: 0 10px
        }

        .achat {
            margin: 20px 0;
            border-radius: 4px;
            background-color: #323C52;
            padding: 5px 20px;
            /* margin: 50px 0 20px 0; */
            color: white;
        }
    </style>
    <nav>
        <div class="navbar">
            <a href="PageNiveaux.php">Selection du niveau</a>
            <a href="boutique.php" <?php if ($nav_en_cours == 'boutique') {
                                        echo ' id="en-cours"';
                                    } ?>>Boutique</a>

        </div>
        <b><?php echo $_SESSION['pieces'] ?> pièces</b>
    </nav>


    <section>
        <?php foreach ($shopItemList as $shopItem) { ?>
            <article>
                <img src="<?= $shopItem->image ?>" alt="" width="100px" height="100px">
                <div class="flex">
                    <h3><?= $shopItem->name ?></h3>
                    <p><?= $shopItem->price ?> pièces</p>
                </div>
                <p class="info"><?= $shopItem->description ?></p>
                <?php
                if ($_SESSION['pieces'] >= $shopItem->price) {
                ?>
                    <form action="" method="post" name="">
                        <input type="hidden" name="id" value="<?= $shopItem->id ?>">
                        <button type="submit" class="achat">Acheter</button>
                    </form>
                <?php
                } else {
                ?>
                    <span class="error">Pas assez de pièces</span>
                <?php
                }
                ?>
            </article>
        <?php } ?>
    </section>
</body>

</html>