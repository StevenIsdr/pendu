<?php

class Niveau{
    public $intensity;
    public $list_words;

    public function __construct(int $intensity, array $list_words){
        $this->intensity = $intensity;
        $this->list_words = $list_words;

    }

    public function chooseWord(){
        $list_size = count($this->list_words) - 1;
        $indice = rand(0,$list_size);
        return $this->list_words[$indice];
    }

}

$liste1 = ["FLUX","JOUR","ANGE","AGILE","ANGLE","FRUIT","LINGE","NOCIF","SEUL","BAVE"];
$liste2 = ["AGNEAU","AMENDE","BANQUE","BUDGET","CUPIDE","ARNAQUE","BALEINE","CALCIUM","FALAISE","GIRAFON"];
$liste3 = ["ADDICTIF","BACHELOR","CALVITIE","CANADIEN","DEVOTION","ATTRACTIF","CIMETIERE","CONCLUANT","CORNICHON","DECORATIF"];
$liste4 = ["BIOLOGISTE","FEUILLETON","HALLUCINER","ALCOOMETRIE","BOUQUINERIE","DETOXIFIANT","ANTIBIOTIQUE","CERTIFICATIF","DERMATOLOGUE","GROSSOPHOBIE"];



$niveau1 = new Niveau(1, $liste1);
echo($niveau1->chooseWord());


?>