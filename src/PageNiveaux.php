<?php
session_start();
$nav_en_cours = 'PageNiveau'
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="PageNiveaux.css">
    <link rel="stylesheet" href="navbar.css">
    <title>Sélection du niveau</title>
</head>

<body>
    <?php
    $niveau1 = ["FLUX", "JOUR", "ANGE", "AGILE", "ANGLE", "FRUIT", "LINGE", "NOCIF", "SEUL", "BAVE"];
    $niveau2 = ["AGNEAU", "AMENDE", "BANQUE", "BUDGET", "CUPIDE", "ARNAQUE", "BALEINE", "CALCIUM", "FALAISE", "GIRAFON"];
    $niveau3 = ["ADDICTIF", "BACHELOR", "CALVITIE", "CANADIEN", "DEVOTION", "ATTRACTIF", "CIMETIERE", "CONCLUANT", "CORNICHON", "DECORATIF"];
    $niveau4 = ["BIOLOGISTE", "FEUILLETON", "HALLUCINER", "ALCOOMETRIE", "BOUQUINERIE", "DETOXIFIANT", "ANTIBIOTIQUE", "CERTIFICATIF", "DERMATOLOGUE", "GROSSOPHOBIE"];


        function chooseWord($list_words){
            return $list_words[array_rand($list_words)];
        }

    ?>

    <div id="content">
        <nav>
            <div class="navbar">
                <a href="PageNiveaux.php" <?php if ($nav_en_cours == 'PageNiveau') {
                                                echo ' id="en-cours"';
                                            } ?>>Selection du niveau</a>
                <a href="boutique.php">Boutique</a>
            </div>
            <b><?php echo $_SESSION['pieces'] ?> pièces</b>
        </nav>


        <h1>Sélectionnez un niveau !</h1>


        <form action="/src/pendu.php" method="POST" id="form">

            <div id="niveaux">

                <div onclick="send_level(1)" class="niveau" id="niv1">
                    <p>Niveau 1</p>
                </div>

                <div onclick="send_level(2)" class="niveau" id="niv2">
                    <p>Niveau 2</p>
                </div>

                <div onclick="send_level(3)" class="niveau" id="niv3">
                    <p>Niveau 3</p>
                </div>

                <div onclick="send_level(4)" class="niveau" id="niv4">
                    <p>Niveau 4</p>
                </div>
            </div>

            <input type="hidden" name="niveau" id="niveau" />
            <input type="hidden" name="mot" id="mot" />
            <input type="hidden" name="firstLetter" id="firstLetter">
            <input type="hidden" name="doublePieces" id="doublePieces">
            <input type="hidden" name="doubleEssaies" id="doubleEssaies">


        </form>
    </div>

    <div class="modal_close" id="modalItem">
        <h2 id="title_modal">Choisissez vos items !</h2>
        <div id="liste_items">

            <div onclick="pickItem(1)" class="item <?php if (in_array(1,$_SESSION['boughtItems'])) echo 'active'; ?>" id="item1">
                
                <p>Afficher la 1ère lettre</p>
            </div>
            <div onclick="pickItem(2)" class="item <?php if (in_array(2,$_SESSION['boughtItems'])) echo 'active'; ?>" id="item2">
                <p>Double tes pièces</p>
            </div>
            <div onclick="pickItem(3)" class="item <?php if (in_array(3,$_SESSION['boughtItems'])) echo 'active'; ?>" id="item3">

                <p>Ajoute des essais</p>
            </div>

        </div>

        <div id="div_btn_modal">
            <button class="btn_modal_jouer" onclick="submit_form()">Lancer la partie !</button>
            <button class="btn_modal_cancel" onclick="close_modal()">Annuler</button>
        </div>
    </div>



    <script>
        function send_level(nb) {
            const level = document.getElementById("niveau");
            const word = document.getElementById("mot");
            const firstLetter = document.getElementById("firstLetter");
            const doublePieces = document.getElementById("doublePieces");
            const doubleEssaies = document.getElementById("doubleEssaies");
            switch (nb) {
                case 1:
                    level.value = 1;
                    word.value = "<?php echo chooseWord($niveau1); ?>"
                    break;
                case 2:
                    level.value = 2;
                    word.value = "<?php echo chooseWord($niveau2); ?>"
                    break;
                case 3:
                    level.value = 3;
                    word.value = "<?php echo chooseWord($niveau3); ?>"
                    break;
                case 4:
                    level.value = 4;
                    word.value = "<?php echo chooseWord($niveau4); ?>"
                    break;
            }
            open_modal();

        }

        function submit_form() {
            document.getElementById("form").submit();
        }

        function open_modal() {
            document.getElementById('modalItem').className = "modal_open";
            document.getElementById('content').classList.add("content_when_modal");
        }

        function close_modal() {
            document.getElementById('modalItem').className = "modal_close";
            document.getElementById('content').classList.remove("content_when_modal");
        }

        function pickItem(nb) {
            switch (nb) {
                case 1:
                    if (document.getElementById('item1').classList.contains('active')) {
                        if (document.getElementById('item1').classList.contains('chosen')) {
                            document.getElementById('item1').classList.remove('chosen');
                            document.getElementById("firstLetter").value = false;
                        } else {
                            document.getElementById('item1').classList.add('chosen');
                            document.getElementById("firstLetter").value = true;
                        }
                    }
                    break;
                case 2:
                    if (document.getElementById('item2').classList.contains('active')) {
                        if (document.getElementById('item2').classList.contains('chosen')) {
                            document.getElementById('item2').classList.remove('chosen');
                        } else {
                            document.getElementById('item2').classList.add('chosen');
                        }
                    }
                    break;
                case 3:
                    if (document.getElementById('item3').classList.contains('active')) {
                        if (document.getElementById('item3').classList.contains('chosen')) {
                            document.getElementById('item3').classList.remove('chosen');
                        } else {
                            document.getElementById('item3').classList.add('chosen');
                        }
                    }
                    break;
            }

        }
    </script>





</body>

</html>