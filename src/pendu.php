<?php
session_start();
$word = $_POST["mot"];
$difficulty = $_POST["niveau"];
$firstLetter = $_POST["firstLetter"];
$doublePieces = $_POST["doublePieces"];
$doubleEssaies = $_POST["doubleEssaies"];
$nav_en_cours = "pendu"
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="pendu.css">
    <link rel="stylesheet" href="navbar.css">
    <title>Pendu</title>
</head>
<body>
<div id="content">
    <nav>
        <div class="navbar">
            <a href="PageNiveaux.php">Selection du niveau</a>
            <a href="boutique.php">Boutique</a>
        </div>
        <b><?php echo $_SESSION['pieces'] ?> pièces</b>
    </nav>


    <p id="wordToFind" style="visibility: hidden"><?php echo $word ?></p>
    <p id="firstLetter" style="visibility: hidden"><?php echo $firstLetter ?></p>
    <p id="doubleEssaies" style="visibility: hidden"><?php echo $doubleEssaies ?></p>
    <p id="difficulty" style="visibility: hidden"><?php echo $difficulty ?></p>
</div>
<div class="pendu">
    <div class="block">
        <div class="essaies">
            <p>Tentative restante : </p>
            <p class="trial" id="trial"></p>
        </div>
        <p>Lettres deja utilisés</p>
        <div id="oldLetters" class="oldLetters">

        </div>
    </div>
    <div class="everything">
        <div class="dessin">
            <canvas id="mon_canvas" width="300" height="300">

                Message pour les navigateurs ne supportant pas encore canvas.

            </canvas>
        </div>
        <h1 id="mot"></h1>
        <div class="intxt">
            <label>
                <input maxlength="1" type="text" id="word">
            </label>
            <button class="button" onclick="pendu()">add</button>
            <p id="alert"></p>
        </div>
    </div>


</div>
    <div class="modal" id="modal-one">
        <div class="modal-bg modal-exit"></div>
        <div class="modal-container">
            <h1>GAGNE</h1>
            <h2>Vous avez trouvé le mot : <?php echo $word ?> </h2>
            <button onclick="closeModal()" class="modal-close modal-exit">X</button>
            <?php
            if ($difficulty === 1) {
                if ($doublePieces) {
                    $_SESSION["pieces"] += 20;
                } else {
                    $_SESSION["pieces"] += 10;
                }
            } else if ($difficulty === 2) {
                if ($doublePieces) {
                    $_SESSION["pieces"] += 40;
                } else {
                    $_SESSION["pieces"] += 20;
                }
            } else if ($difficulty === 3) {
                if ($doublePieces) {
                    $_SESSION["pieces"] += 60;
                } else {
                    $_SESSION["pieces"] += 30;
                }
            } else if ($difficulty === 4) {
                if ($doublePieces) {
                    $_SESSION["pieces"] += 100;
                } else {
                    $_SESSION["pieces"] += 50;
                }
            }
            ?>
        </div>
    </div>

    <div class="modal" id="modal-two">
        <div class="modal-bg modal-exit"></div>
        <div class="modal-container">
            <h1>PERDU</h1>
            <h2>Vous n'avez pas trouvé le mot qui était :<?php echo $word ?> </h2>
            <button onclick="closeModal2()" class="modal-close modal-exit">X</button>
        </div>
    </div>
</div>
    <script src="pendu.js"></script>
</body>
</html>