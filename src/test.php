<?php

use PHPUnit\Framework\TestCase;
require("user.php");

class test extends TestCase
{
    public function testOne()
    {
        $user = new User();
        $result = $user->connect("steven","steven");
        $this->assertTrue($result);
    }

    /**
     * @depends testOne
     */
    public function testTwo()
    {
        $user = new User();
        $result = $user->connect("steven","stessven");
        $this->assertFalse($result);
    }
}
